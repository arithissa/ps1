# Arquitetura do Playstation 1

Trabalho da disciplina Arquitetura de Computadores apresentando a arquitetura do console Playstation 1.

## Especificações Técnicas

A CPU do PlayStation 1 era baseada num microprocessador MIPS R3000A, utilizando um microprocessador MIPS R3051 de 32 bits e arquitetura RISC, com 5kb de cache L1, adaptado com 3 coprocessadores em vez de 4, sendo estes:
- Geometry Transformation Engine (GTE)
- Motion Decoder (MDEC)
- System Control Coprocessor (Cop0).

Possui uma Unidade Lógica e Aritmética (ULA) com um shifter (ou deslocador), operando com frequência de 33.87 MHz e performance operacional de 30 MIPS.

Trabalha com 32 registradores gerais e 2 registradores de 32 bits de multiplicação/divisão.
Um dos registradores gerais é sempre zero (R0) - padrão de processadores baseados em arquitetura RISC.

Utiliza-se um Pipeline de 5 Níveis, em que a execução das instruções é dividida em cinco estágios. São assimiladas até cinco instruções de uma vez, cada uma em um estágio do processo, de modo que todos os recursos do processador sejam aproveitados simultaneamente, aumentando a velocidade de execução de instruções.

##### Sobre os 3 coprocessadores:

System Control Coprocessor <br />
Esse coprocessador é padrão das CPUs da MIPS. Como o nome diz, sua função principal é auxiliar no controle do sistema. Permite acesso direto ao cache de dados e ao cache de instruções, é responsável por assimilar, gerenciar e interromper, caso necessário, determinadas instruções de execução.

Geometry Transformation Engine (GTE) <br />
O papel desse processador é acelerar os cálculos vetoriais e matriciais a serem feitos com o objetivo de auxiliar nos recursos gráficos do PlayStation, com performance operacional de 66 MIPS. Sua alta velocidade de cálculos permite a rápida projeção de iluminação, gráficos 3D e é imprescindível do ponto de vista posicional e geométrico dos gráficos, trabalhando com alta precisão de coordenadas.

Motion Decoder (MDEC) <br />
Mais um coprocessador com funções relacionadas à projeção dos gráficos do PlayStation, este é responsável por converter determinadas estruturas de dados (macroblocks) em um formato que a GPU (Graphics Processing Unit) consiga assimilar e projetar (VRAM). Trabalha com performance operacional de 80 MIPS

##### Placa gráfica:

A GPU de 32 bits foi desenvolvida pela Toshiba, tratava de desenhar os polígonos e as texturas e lançava a imagem na tela.
Possui framebuffer (faz o mapeamento dos pixels na tela) ajustável de 1024x512, tratava efeitos de profundidade, transparência, névoa, fontes de luz, etc. 
O dispositivo pode renderizar tanto de forma intercalada como progressiva.

A placa gráfica utiliza 24-bits de precisão pra definir as cores e 32 níveis de transparência além de suportar os efeitos de rotações de objetos tridimensionais

## Bibliografia

##### ORIGEM
https://www.oficinadanet.com.br/games/35742-a-historia-do-playstation <br/>

##### DUALSHOCK E MEMORY CARD
https://pt.wikipedia.org/wiki/DualShock <br/>
https://pt.wikipedia.org/wiki/Dual_Analog_Controller <br/>
https://www.oficinadanet.com.br/games/35742-a-historia-do-playstation <br/>


##### PRINCIPAIS APLICAÇÕES
https://tecnoblog.net/meiobit/420340/quem-lembra-os-10-melhores-jogos-do-playstation-1/ <br/>

##### ARQUITETURA 
https://www.copetti.org/writings/consoles/playstation/ <br/>


##### ORGANIZAÇÃO EXTERNA DO PS1 (modelos)
https://pt.wikipedia.org/wiki/PlayStation_(console) <br/>
https://forum.outerspace.com.br/index.php?threads/uma-dúvida-sobre-o-playstation-1.416104/ <br/>


##### ORGANIZAÇÃO INTERNA DO PS1 
https://www.theverge.com/circuitbreaker/2019/12/3/20993374/playstation-sony-ifixit-teardown-simple-hardware-internal-components-cd-drive <br/>
https://pt.ifixit.com/Teardown/Sony+PlayStation+Teardown/128089?lang=en <br/>
https://pt.ifixit.com/Teardown/Sony+PlayStation+SCPH-9002+Teardown/2631?lang=en <br/>
https://pt.ifixit.com/Device/PlayStation <br/>


##### CURIOSIDADE (NASA) 
https://canaltech.com.br/ciencia/NASA-utiliza-CPU-do-PlayStation-original-para-guiar-sonda-a-Plutao/ <br/>

